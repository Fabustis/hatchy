<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    /**
     * @Route("/", name="app_landing")
     */
    public function index(ChartBuilderInterface $chartBuilder): Response
    {
        return $this->render('base/index.html.twig', [
            'controller_name' => 'LandingController',
        ]);
    }
}
