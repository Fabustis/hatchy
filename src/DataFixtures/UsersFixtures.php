<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTimeImmutable;
use App\Entity\Project;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UsersFixtures extends Fixture
{
    private $userPasswordHasherInterface;

    // need to inject UserPasswordHasherInterface in a constructor 
    public function __construct (UserPasswordHasherInterface $userPasswordHasherInterface) 
    {
        $this->userPasswordHasherInterface = $userPasswordHasherInterface;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('jo@jo.fr')
            ->setPassword(
                $this->userPasswordHasherInterface->hashPassword(
                    $user, '123456789'
                )
            );
        
        $manager->persist($user);
        $manager->flush();
        
        $project = new Project();
        $project->setName('Project Fixture')
                ->setCreatedAt(new DateTimeImmutable('NOW'))
                ->setDeadLine(new DateTimeImmutable('2022-12-31'));
        
        $manager->persist($project);
        $manager->flush();

    }
}
