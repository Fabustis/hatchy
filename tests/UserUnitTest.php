<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Project;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        //$project=new Project();
        $user = new User();
        $user   ->setEmail("true.test@test.fr")
                ->setPassword("password");

        $this->assertTrue($user->getEmail() === "true.test@test.fr");
        $this->assertTrue($user->getPassword() === "password");
        //$this->assertTrue($user->getProject() === $project);
    }
    public function testIsFalse()
    {
        $user = new User();
        $user   ->setEmail("true.test@test.fr")
                ->setPassword("password");

        $this->assertFalse($user->getEmail() === "false.test@test.fr");
        $this->assertFalse($user->getPassword() === "FailPWD");
        $this->assertFalse($user->getProject() === "FailProject");
    }
    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        //$this->assertEmpty($user->getProject());
    }
}
