<?php

namespace App\Tests;

use App\Entity\Project;
use DateTime;
use Monolog\DateTimeImmutable;
use PHPUnit\Framework\TestCase;
/**
 * @group project
 * @return void
 */

class ProjectUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $project =  new Project();
        $datetime = new DateTimeImmutable('now');
        $date = new DateTime();
        $project->setName("Organiser l'architecture")
                ->setDescription("lorem ipsum x200000")
                ->setCreatedAt($datetime)
                ->setDeadline($date);

        $this->assertTrue($project->getName() === "Organiser l'architecture");
        $this->assertTrue($project->getDescription() === "lorem ipsum x200000");
        $this->assertTrue($project->getCreatedAt() === $datetime);
        $this->assertTrue($project->getDeadline() === $date);
    }
    public function testIsFalse()
    {
        $project =  new Project();
        $datetime = new DateTimeImmutable('now');
        $project->setName("Organiser l'architecture")
                ->setDescription("lorem ipsum x200000")
                ->setCreatedAt($datetime)
                ->setDeadline(new DateTime('2022-11-01'));

        $this->assertFalse($project->getName() === "Manger les crêpes");
        $this->assertFalse($project->getDescription() === "lorem dapsum");
        $this->assertFalse($project->getCreatedAt() === '9999-11-01');
        $this->assertFalse($project->getDeadline() === '9999-11-01');
    }
    public function testIsEmpty()
    {
        $project =  new Project();

        $this->assertEmpty($project->getName());
        $this->assertEmpty($project->getDescription());
        $this->assertEmpty($project->getCreatedAt());
        $this->assertEmpty($project->getDeadline());
    }
}
