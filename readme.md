# HATCHY : Description
Inspired by the number 8 in Japanese, hachi, Hatchy evokes the management of many tasks of a project. Represented by an octopus logo that has 8 tentacles.

It is above all a poersonnal project to progress in web development but with the help of the community.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Pre-required

* Composer
* PHP 7.4
* Symfony CLI
* Docker-compose

## Run/Start

composer install
npm install
npm run build 

docker-compose up -d
symfony serve -d
or
php -S localhost:8000 -t public/

(or from Docker container 
docker exec -it www_docker_gitlab bash)

## Tests

php bin/phpunit [--group=xxxxx] --testdox


## TODO
- Fixtures Fakers Users and Projects
- Chart.js